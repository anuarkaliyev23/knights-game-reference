package kz.mathncode.anuarkaliyev23.knights.exceptions;

public class KnightsException extends RuntimeException {
    public KnightsException() {
    }

    public KnightsException(String message) {
        super(message);
    }

    public KnightsException(String message, Throwable cause) {
        super(message, cause);
    }
}
