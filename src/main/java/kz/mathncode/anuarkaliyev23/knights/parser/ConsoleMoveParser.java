package kz.mathncode.anuarkaliyev23.knights.parser;

import kz.mathncode.anuarkaliyev23.knights.coordinate.Horizontal;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Position;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Vertical;
import kz.mathncode.anuarkaliyev23.knights.exceptions.ParserException;
import kz.mathncode.anuarkaliyev23.knights.move.Move;

import java.util.Scanner;

public class ConsoleMoveParser implements MoveParser {
    private Scanner scanner;

    public ConsoleMoveParser(Scanner scanner) {
        this.scanner = scanner;
    }

    public Position parsePosition(String positionLine) {
        String upperCased = positionLine.toUpperCase();
        String vertical = upperCased.substring(0, 1);
        String horizontal = upperCased.substring(1);

        Vertical parsedVertical = Vertical.valueOf(vertical);
        Horizontal parsedHorizontal = Horizontal.values()[Integer.parseInt(horizontal) - 1];
        return new Position(parsedVertical, parsedHorizontal);
    }

    /**
     * Parses next move of a given format:
     *
     * e2 - e4
     * etc.
     * */
    @Override
    public Move nextMove() {
        String line = scanner.nextLine().trim();
        try {
            String[] split = line.split(DELIMITER);

            Position start = parsePosition(split[0].trim());
            Position target = parsePosition(split[1].trim());
            return new Move(start, target);
        } catch (Exception e) {
            throw new ParserException("Error occurred while parsing", e);
        }
    }

    private static final String DELIMITER = "-";
}
