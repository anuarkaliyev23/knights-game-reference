package kz.mathncode.anuarkaliyev23.knights.coordinate;

public enum Horizontal {
    I(1, "\uff11"),
    II(2, "\uff12"),
    III(3, "\uff13"),
    IV(4, "\uff14"),
    V(5, "\uff15"),
    VI(6, "\uff16"),
    VII(7, "\uff17"),
    VIII(8, "\uff18");

    private int horizontalIndex;
    private String unicode;

    Horizontal(int horizontalIndex, String unicode) {
        this.horizontalIndex = horizontalIndex;
        this.unicode = unicode;
    }

    public int getHorizontalIndex() {
        return horizontalIndex;
    }

    public void setHorizontalIndex(int horizontalIndex) {
        this.horizontalIndex = horizontalIndex;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }
}
