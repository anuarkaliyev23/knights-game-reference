package kz.mathncode.anuarkaliyev23.knights.game;

import kz.mathncode.anuarkaliyev23.knights.coordinate.Horizontal;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Position;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Vertical;
import kz.mathncode.anuarkaliyev23.knights.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.knights.exceptions.KnightNotFoundException;
import kz.mathncode.anuarkaliyev23.knights.exceptions.KnightsException;
import kz.mathncode.anuarkaliyev23.knights.move.Move;
import kz.mathncode.anuarkaliyev23.knights.piece.Knight;

import java.util.List;

public class Game {
    private final List<Knight> knights;
    private boolean isWhiteMove;

    public Game(List<Knight> knights, boolean isWhiteMove) {
        this.knights = knights;
        this.isWhiteMove = isWhiteMove;
    }

    public List<Knight> getKnights() {
        return knights;
    }

    public boolean isWhiteMove() {
        return isWhiteMove;
    }

    public void setWhiteMove(boolean whiteMove) {
        isWhiteMove = whiteMove;
    }

    public Knight findByPositionOrNull(Position position) {
        for (Knight k : knights) {
            if (k.getPosition().equals(position)) {
                return k;
            }
        }
        return null;
    }

    public Knight findByPosition(Position position) throws KnightNotFoundException {
        Knight knight = findByPositionOrNull(position);
        if (knight == null) {
            throw new KnightNotFoundException();
        } else {
            return knight;
        }
    }

    public Knight findByColor(boolean isWhite) {
        for (Knight knight : knights) {
            if (knight.isWhite() == isWhite)
                return knight;
        }
        throw new KnightsException(String.format("There are no knights of color {%s}", isWhite));
    }

    public void handle(Move move) {
        Knight knightToBeMoved = findByPositionOrNull(move.getStart());
        if (knightToBeMoved == null) {
            throw new ImpossibleMoveException(String.format("There is no knight at position {%s} to be moved", move.getStart()), move);
        }

        if (knightToBeMoved.isWhite() != isWhiteMove) {
            throw new ImpossibleMoveException(String.format("Cannot move as {%s}, since now is {%s} move", knightToBeMoved.isWhite(), isWhiteMove), move);
        }

        Knight possibleTarget = findByPositionOrNull(move.getTarget());
        if (possibleTarget != null) {
            throw new ImpossibleMoveException(String.format("There is a knight at position {%s}. Eating is forbidden in this game", move.getTarget()), move);
        }

        if (knightToBeMoved.possibleMove(move.getTarget())) {
            knightToBeMoved.setPosition(move.getTarget());
            isWhiteMove = !isWhiteMove;
        } else {
            throw new ImpossibleMoveException(move);
        }
    }

    public GameStatus status() {
        Knight whiteKnight = findByColor(true);
        Knight blackKnight = findByColor(false);

        if (whiteKnight.getPosition().equals(new Position(Vertical.A, Horizontal.VIII)))
            return GameStatus.WHITE_WON;
        if (blackKnight.getPosition().equals(new Position(Vertical.H, Horizontal.I))) {
            return GameStatus.BLACK_WON;
        }
        return GameStatus.IN_PROCESS;
    }
}
