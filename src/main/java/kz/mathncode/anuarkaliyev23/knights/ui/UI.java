package kz.mathncode.anuarkaliyev23.knights.ui;

public interface UI {
    void show();
    void nextMove();
}
