package kz.mathncode.anuarkaliyev23.knights.move;

import kz.mathncode.anuarkaliyev23.knights.coordinate.Position;

import java.util.Objects;

public class Move {
    private Position start;
    private Position target;

    public Move(Position start, Position target) {
        this.start = start;
        this.target = target;
    }

    public Position getStart() {
        return start;
    }

    public void setStart(Position start) {
        this.start = start;
    }

    public Position getTarget() {
        return target;
    }

    public void setTarget(Position target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return Objects.equals(start, move.start) && Objects.equals(target, move.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, target);
    }
}
