package kz.mathncode.anuarkaliyev23.knights.exceptions;

import kz.mathncode.anuarkaliyev23.knights.move.Move;

public class ImpossibleMoveException extends KnightsException {
    private final Move move;

    public ImpossibleMoveException(Move move) {
        this.move = move;
    }

    public ImpossibleMoveException(String message, Move move) {
        super(message);
        this.move = move;
    }

    public ImpossibleMoveException(String message, Throwable cause, Move move) {
        super(message, cause);
        this.move = move;
    }

    public Move getMove() {
        return move;
    }
}
