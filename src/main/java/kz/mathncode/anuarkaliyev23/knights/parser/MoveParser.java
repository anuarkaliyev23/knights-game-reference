package kz.mathncode.anuarkaliyev23.knights.parser;

import kz.mathncode.anuarkaliyev23.knights.move.Move;

public interface MoveParser {
    Move nextMove();
}
