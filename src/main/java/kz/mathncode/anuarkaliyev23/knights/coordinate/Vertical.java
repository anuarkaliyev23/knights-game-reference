package kz.mathncode.anuarkaliyev23.knights.coordinate;

public enum Vertical {
    A(1, "\uff21"),
    B(2, "\uff22"),
    C(3, "\uff23"),
    D(4, "\uff24"),
    E(5, "\uff25"),
    F(6, "\uff26"),
    G(7, "\uff27"),
    H(8, "\uff28");

    private int verticalIndex;
    private String unicode;

    Vertical(int verticalIndex, String unicode) {
        this.verticalIndex = verticalIndex;
        this.unicode = unicode;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }

    public int getVerticalIndex() {
        return verticalIndex;
    }
    public void setVerticalIndex(int verticalIndex) {
        this.verticalIndex = verticalIndex;
    }
}
