package kz.mathncode.anuarkaliyev23.knights.ui;

import kz.mathncode.anuarkaliyev23.knights.coordinate.Horizontal;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Position;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Vertical;
import kz.mathncode.anuarkaliyev23.knights.game.Game;
import kz.mathncode.anuarkaliyev23.knights.move.Move;
import kz.mathncode.anuarkaliyev23.knights.parser.MoveParser;
import kz.mathncode.anuarkaliyev23.knights.piece.Knight;

public class ConsoleUI implements UI {
    private MoveParser parser;
    private Game game;


    public ConsoleUI(MoveParser parser, Game game) {
        this.parser = parser;
        this.game = game;
    }

    public MoveParser getParser() {
        return parser;
    }

    public void setParser(MoveParser parser) {
        this.parser = parser;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        for (Horizontal horizontal : Horizontal.values()) {
            System.out.print(horizontal.getUnicode());
            System.out.print(FULL_WIDTH_VERTICAL_LINE);
            for (Vertical vertical : Vertical.values()) {
                Knight knight = game.findByPositionOrNull(new Position(vertical, horizontal));
                if (knight == null) {
                    System.out.print(FULL_WIDTH_UNDERSCORE);
                    System.out.print(FULL_WIDTH_VERTICAL_LINE);
                } else {
                    System.out.print(toUnicode(knight));
                    System.out.print(FULL_WIDTH_VERTICAL_LINE);
                }
            }
            System.out.println();
        }

        System.out.print(FULL_WIDTH_SPACE);
        System.out.print(FULL_WIDTH_SPACE);


        for (Vertical vertical : Vertical.values()) {
            System.out.print(vertical.getUnicode());
            System.out.print(FULL_WIDTH_SPACE);
        }
        System.out.println();
    }

    @Override
    public void nextMove() {
        Move next = parser.nextMove();
        game.handle(next);
    }

    private String toUnicode(Knight knight) {
        if (knight.isWhite()) {
            return WHITE_KNIGHT_UNICODE;
        } else {
            return BLACK_KNIGHT_UNICODE;
        }
    }

    public static String WHITE_KNIGHT_UNICODE = "\u2658";
    public static String BLACK_KNIGHT_UNICODE = "\u265e";

    public static String FULL_WIDTH_UNDERSCORE = "\uff3f";
    public static String FULL_WIDTH_VERTICAL_LINE = "\uff5c";
    public static String FULL_WIDTH_SPACE = "\u3000";
}
