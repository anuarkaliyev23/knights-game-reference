package kz.mathncode.anuarkaliyev23.knights.piece;

import kz.mathncode.anuarkaliyev23.knights.coordinate.Position;

import java.util.Objects;

public class Knight {
    private Position position;
    private boolean isWhite;

    public Knight(Position position, boolean isWhite) {
        this.position = position;
        this.isWhite = isWhite;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isWhite() {
        return isWhite;
    }

    public void setWhite(boolean white) {
        isWhite = white;
    }

    public boolean possibleMove(Position target) {
        int deltaVertical = Math.abs(target.getVertical().getVerticalIndex() - position.getVertical().getVerticalIndex());
        int deltaHorizontal = Math.abs(target.getHorizontal().getHorizontalIndex() - position.getHorizontal().getHorizontalIndex());

        return deltaHorizontal * deltaVertical == MULTIPLIER_VALIDITY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Knight knight = (Knight) o;
        return isWhite == knight.isWhite && Objects.equals(position, knight.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, isWhite);
    }

    private static final int MULTIPLIER_VALIDITY = 2;
}
