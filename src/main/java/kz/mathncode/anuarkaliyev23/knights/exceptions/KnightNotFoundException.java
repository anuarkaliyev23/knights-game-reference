package kz.mathncode.anuarkaliyev23.knights.exceptions;

public class KnightNotFoundException extends KnightsException {
    public KnightNotFoundException() {
    }

    public KnightNotFoundException(String message) {
        super(message);
    }

    public KnightNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
