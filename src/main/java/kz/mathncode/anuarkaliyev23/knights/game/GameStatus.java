package kz.mathncode.anuarkaliyev23.knights.game;

public enum GameStatus {
    WHITE_WON,
    BLACK_WON,
    IN_PROCESS;
}
