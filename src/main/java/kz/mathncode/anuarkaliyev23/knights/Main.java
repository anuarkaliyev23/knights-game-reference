package kz.mathncode.anuarkaliyev23.knights;

import kz.mathncode.anuarkaliyev23.knights.coordinate.Horizontal;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Position;
import kz.mathncode.anuarkaliyev23.knights.coordinate.Vertical;
import kz.mathncode.anuarkaliyev23.knights.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.knights.exceptions.ParserException;
import kz.mathncode.anuarkaliyev23.knights.game.Game;
import kz.mathncode.anuarkaliyev23.knights.game.GameStatus;
import kz.mathncode.anuarkaliyev23.knights.parser.ConsoleMoveParser;
import kz.mathncode.anuarkaliyev23.knights.parser.MoveParser;
import kz.mathncode.anuarkaliyev23.knights.piece.Knight;
import kz.mathncode.anuarkaliyev23.knights.ui.ConsoleUI;
import kz.mathncode.anuarkaliyev23.knights.ui.UI;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Knight whiteKnight = new Knight(new Position(Vertical.H, Horizontal.I), true);
        Knight blackKnight = new Knight(new Position(Vertical.A, Horizontal.VIII), false);
        List<Knight> knights = new ArrayList<>();
        knights.add(whiteKnight);
        knights.add(blackKnight);

        Game game = new Game(knights, true);
        MoveParser parser = new ConsoleMoveParser(new Scanner(System.in));

        UI ui = new ConsoleUI(parser, game);

        while (game.status() == GameStatus.IN_PROCESS) {
            try {
                ui.show();
                ui.nextMove();
            } catch (ImpossibleMoveException | ParserException e) {
                System.out.println(e.getMessage());
                System.out.println("This move is impossible. Please, try again");
            } catch (Exception e) {
                System.out.println("Unexpected error");
                System.exit(1);
            }
        }
    }
}
