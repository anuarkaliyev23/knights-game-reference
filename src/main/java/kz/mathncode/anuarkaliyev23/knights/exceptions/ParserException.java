package kz.mathncode.anuarkaliyev23.knights.exceptions;

public class ParserException extends KnightsException {
    public ParserException() {
    }

    public ParserException(String message) {
        super(message);
    }

    public ParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
